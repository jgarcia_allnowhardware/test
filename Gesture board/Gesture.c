
#include "GestureDefs.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "nrf_drv_twi.h"
#include "app_util_platform.h "
#include "nrf_delay.h"
#include "nrf_gpio.h"
static bool TS_RDY;
const nrf_drv_twi_t m_twi_MGSC3130 = NRF_DRV_TWI_INSTANCE(0);
static 	uint32_t error;
//Global variables
s_Gesture mGesture;
uint8_t raw_data[132];
uint8_t raw_datarx[132];

volatile bool MGSC3130_xfer_done;


//void GestIC_Read(char *msgData, char msgLen){
//  I2C1_Start();
//  I2C1_Read(MGC_ADDR0, msgData, msgLen , END_MODE_STOP);
//  delay_us(200);
//}

//void GestIC_Write(char *msgData, char msgLen) {
//  I2C1_Start();
//  I2C1_Write(MGC_ADDR0,raw_data,msgLen,END_MODE_STOP);
//  delay_us(200);
//}

void twi_handler_MGSC3130(nrf_drv_twi_evt_t const * p_event, void * p_context)
{   
    MGSC3130_xfer_done = true;
}

//Configuracion del TWI
void twi_init (void)
{
    ret_code_t err_code;
	const nrf_drv_twi_config_t twi_MGSC3130_config = {
       .scl                = 12,
       .sda                = 11,
       .frequency          = NRF_TWI_FREQ_400K,
       .interrupt_priority = APP_IRQ_PRIORITY_LOW
    };
		err_code = nrf_drv_twi_init(&m_twi_MGSC3130, &twi_MGSC3130_config, twi_handler_MGSC3130, NULL);
		
		nrf_drv_twi_enable(&m_twi_MGSC3130);
}



void MGSC3130_envia(uint8_t numaenviar)
{
	nrf_drv_twi_tx(&m_twi_MGSC3130, MGC_ADDR0, raw_data, numaenviar, false);
	nrf_delay_us(200);
}

void MGSC3130_recibe(uint8_t numarecibir)
{

	error =nrf_drv_twi_rx(&m_twi_MGSC3130, MGC_ADDR0, raw_datarx, numarecibir);
	nrf_delay_us(200);
}


//**********************************************************************************************************************
//                                            SET_RUNTIME_PARAMETERS (0XA2)
//**********************************************************************************************************************


char setAirWheel(char ENABLE) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x90; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  if (ENABLE) {
    raw_data[8] = 0x20; //MASK
  }else{
    raw_data[8] = 0x00; //MASK
  }
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x20;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
  MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

char setTouchDetection(char ENABLE) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x97; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  if (ENABLE) {
    raw_data[8] = 0x08; //MASK
  }else{
    raw_data[8] = 0x00; //MASK
  }
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x08;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

char setApproachDetection(char ENABLE) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x97; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  if (ENABLE) {
    raw_data[8] = 0x01; //MASK
  }else{
    raw_data[8] = 0x00; //MASK
  }
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x01;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

/*
This command is not anymore supported starting from V1.0 release.
*/
char setApproachDetection2(char ENABLE) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x81; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  if (ENABLE) {
    raw_data[8] = 0x01; //MASK
  }else{
    raw_data[8] = 0x00; //MASK
  }
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x01;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

/* Parameters:
0 - Enable All Gestures
1 - Enable Only Flick Gestures
2 - Enable in Addition Circles
*/
char setGestureProcessingHMM(char PARAM) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x85; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  switch (PARAM) {
    case 1:
      raw_data[8] = 0x1F;
      raw_data[12] = 0x7F;
      break;
    case 2:
      raw_data[8] = 0x60;
      raw_data[12] = 0x60;
      break;
    default:
      raw_data[8] = 0x7F;
      raw_data[12] = 0x7F;
  }
  //Argument 0
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}


char setCalibrationOperationMode(char ENABLE) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x80; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  if (ENABLE) {
    raw_data[8] = 0x00; //MASK
  }else{
    raw_data[8] = 0x3F; //MASK
  }
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x3F;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

/* Parameters:
0 - Enable All Data
1 - Enable DSP, Gestures and Noise Power
2 - Enable Only Data: Noise (others not changed)
3 - Disable Only Data: CIC (others not changed)
*/
char setDataOutputEnableMask(char PARAM) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0xA0; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  switch (PARAM) {
    case 1:
      raw_data[8] = 0x23;
      raw_data[9] = 0x00;
      raw_data[12] = 0x3F;
      raw_data[13] = 0x18;
      break;
    case 2:
      raw_data[8] = 0x10;
      raw_data[9] = 0x00;
      raw_data[12] = 0x10;
      raw_data[13] = 0x00;
      break;
     case 3:
      raw_data[8] = 0x00;
      raw_data[9] = 0x00;
      raw_data[12] = 0x00;
      raw_data[13] = 0x08;
      break;
    default:
      raw_data[8] = 0x3F;
      raw_data[9] = 0x18;
      raw_data[12] = 0x3F;
      raw_data[13] = 0x18;
  }
  //Argument 0
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

/* Parameters:
0 - Lock All Data
1 - Lock DSP, Gestures and Noise Power
2 - Lock Only Data: Noise (others not changed)
3 - UnLock Only Data: CIC (others not changed)
*/
char setDataOutputLockMask(char PARAM) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0xA1; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  switch (PARAM) {
    case 1:
      raw_data[8] = 0x23;
      raw_data[9] = 0x00;
      raw_data[12] = 0x3F;
      raw_data[13] = 0x18;
      break;
    case 2:
      raw_data[8] = 0x10;
      raw_data[9] = 0x00;
      raw_data[12] = 0x10;
      raw_data[13] = 0x00;
      break;
     case 3:
      raw_data[8] = 0x00;
      raw_data[9] = 0x00;
      raw_data[12] = 0x00;
      raw_data[13] = 0x08;
      break;
    default:
      raw_data[8] = 0x3F;
      raw_data[9] = 0x18;
      raw_data[12] = 0x3F;
      raw_data[13] = 0x18;
  }
  //Argument 0
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}


/* Parameters:
0 - Request All Data
1 - Request DSP, Gestures and Noise Power
2 - Request Only Data: Noise
*/
char setDataOutputRequestkMask(char PARAM) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0xA2; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  switch (PARAM) {
    case 1:
      raw_data[8] = 0x23;
      raw_data[9] = 0x00;
      raw_data[12] = 0x3F;
      raw_data[13] = 0x18;
      break;
    case 2:
      raw_data[8] = 0x10;
      raw_data[9] = 0x00;
      raw_data[12] = 0x10;
      raw_data[13] = 0x00;
      break;
    default:
      raw_data[8] = 0x3F;
      raw_data[9] = 0x18;
      raw_data[12] = 0x3F;
      raw_data[13] = 0x18;
  }
  //Argument 0
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

char setGestureInProgressFlag(char ENABLE) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0xA3; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  if (ENABLE) {
    raw_data[8] = 0x01; //MASK
  }else{
    raw_data[8] = 0x00; //MASK
  }
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x01;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}


/* Parameters:
0 - Store RTPs for AFE
1 - Store RTPs for DSP
2 - Store RTPs for System
*/
char setMakePersistent(char PARAM) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x00; //Parameter ID
  raw_data[5] = 0xFF;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  switch (PARAM) {
    case 1:
      raw_data[8] = 0x01;
      break;
    case 2:
      raw_data[8] = 0x02;
      break;
    default:
      raw_data[8] = 0x00;
  }
  //Argument 0
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x00;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}


/* Parameters:
0 - Force Calibration
1 - Enter Deep Sleep 1
2 - Enter Deep Sleep 2
*/
char setTrigger(char PARAM) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x00; //Parameter ID
  raw_data[5] = 0x10;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  switch (PARAM) {
    case 1:
      raw_data[8] = 0x02;
      break;
    case 2:
      raw_data[8] = 0x03;
      break;
    default:
      raw_data[8] = 0x00;
  }
  //Argument 0
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x00;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
    MGSC3130_envia(0x10);
  //ToDo: Read and parse error
  return 1;
}

void setElectrodeMap(char Electrode, char Channel) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = Electrode; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  raw_data[8] = Channel;
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = 0x00;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;
  //Send data
   MGSC3130_envia(0x10);
  
}

void TransFreqSelect(char FreqCnt, unsigned long FreqOrder) {
  ////////////////////////////////////////
  //             header
  ////////////////////////////////////////
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  ////////////////////////////////////////
  //             payload
  ////////////////////////////////////////
  raw_data[4] = 0x82; //Parameter ID
  raw_data[5] = 0x00;
  //reserved
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  //Argument 0
  raw_data[8] = FreqCnt;
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  //Argument 1
  raw_data[12] = (char)(FreqOrder & 0x000000FF);
  raw_data[13] = (char)((FreqOrder >> 8) & 0x000000FF);
  raw_data[14] = (char)((FreqOrder >> 16) & 0x000000FF);
  raw_data[15] = (char)((FreqOrder >> 24) & 0x000000FF);
  //Send data
    MGSC3130_envia(0x10);
}


void setEnableAllGestures() {
  //header
  raw_data[0] = 0x10; //raw data size
  raw_data[1] = 0x00; //flags
  raw_data[2] = 0x00; //seq
  raw_data[3] = 0xA2; //ID
  //payload
  raw_data[4] = 0x85; //param ID
  raw_data[5] = 0x00;
  
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;

  raw_data[8] = 0x7F; //DATA OUTPUT MASK
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;

  raw_data[12] = 0x7F;
  raw_data[13] = 0x00;
  raw_data[14] = 0x00;
  raw_data[15] = 0x00;

    MGSC3130_envia(0x10);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                           REQUEST FW_VERSION_INFO FROM MGC3130
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void getFWInfo() {
  char i;
	nrf_gpio_pin_write(MGC_RST,0);
//	MGC_RST = 0;
  //send msg to request fw info
  raw_data[0] = 0x0C;
  raw_data[1] = 0x00;
  raw_data[2] = 0x00;
  raw_data[3] = 0x06;
  raw_data[4] = 0x83;
  raw_data[5] = 0x00;
  raw_data[6] = 0x00;
  raw_data[7] = 0x00;
  raw_data[8] = 0x00;
  raw_data[9] = 0x00;
  raw_data[10] = 0x00;
  raw_data[11] = 0x00;
  
	nrf_gpio_pin_write(MGC_RST,1);
	nrf_delay_ms(10);
//	nrf_gpio_cfg_output(MGC_RDY_IN);
//	nrf_gpio_pin_write(MGC_RDY_IN,0);
//  MGC_RST = 1;
   MGSC3130_envia(0x0C);
//	 nrf_gpio_cfg(MGC_RDY_IN,NRF_GPIO_PIN_DIR_INPUT,NRF_GPIO_PIN_INPUT_CONNECT,NRF_GPIO_PIN_NOPULL,NRF_GPIO_PIN_S0D1,NRF_GPIO_PIN_SENSE_LOW);
//	while (nrf_gpio_pin_read(MGC_RDY_IN))
//	nrf_gpio_cfg_output(MGC_RDY_IN); 
//	nrf_gpio_pin_write(MGC_RDY_IN,0);
		nrf_delay_ms(100);
  MGSC3130_recibe(0x84);
  nrf_delay_ms(10);
  //Parse values
  mGesture.FWVersionInfo.FWValid = raw_datarx[4];
  mGesture.FWVersionInfo.HWRev[0] = raw_datarx[5];
  mGesture.FWVersionInfo.HWRev[1] = raw_datarx[6];
  mGesture.FWVersionInfo.ParameterStartAddr = raw_datarx[7];
  mGesture.FWVersionInfo.LibraryLoaderVersion[0] = raw_datarx[8];
  mGesture.FWVersionInfo.LibraryLoaderVersion[1] = raw_datarx[9];
  mGesture.FWVersionInfo.LibraryLoaderVersion[3] = raw_datarx[10];
  mGesture.FWVersionInfo.FwStartAddr = raw_datarx[11];
  //
//	nrf_gpio_pin_write(MGC_RDY_OUT,1);
//	nrf_gpio_cfg(MGC_RDY_IN,NRF_GPIO_PIN_DIR_INPUT,NRF_GPIO_PIN_INPUT_CONNECT,NRF_GPIO_PIN_PULLUP,NRF_GPIO_PIN_S0D1,NRF_GPIO_PIN_SENSE_HIGH);
  for (i=0; i<120; i++) {
     mGesture.FWVersionInfo.FwVersion[i] = raw_data[i+12];
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                     READ AND PARSE VALUES FROM GESTURE BOARD
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void updateRawData() {
  MGSC3130_recibe(64);
}

void updateGestureData() {
		nrf_delay_ms(100);
//  while (nrf_gpio_pin_read(MGC_RDY_IN))
		//	GPIO_Digital_Output (&GPIOD_BASE, _GPIO_PINMASK_10);
		nrf_gpio_cfg_output(MGC_RDY_OUT);
    nrf_gpio_pin_write(MGC_RDY_OUT,0);                                   //Assert TS

    MGSC3130_recibe(32);
    //Parse values
    mGesture.DataOut.Len = raw_datarx[0];
    mGesture.DataOut.Flags = raw_datarx[1];
    mGesture.DataOut.Seq = raw_datarx[2];
    mGesture.DataOut.Id = raw_datarx[3];
    mGesture.DataOut.ConfigMask = (raw_datarx[5] << 8) + raw_datarx[4];
    mGesture.DataOut.TimeStamp = raw_datarx[6];
    mGesture.DataOut.SystemInfo = raw_datarx[7];
    mGesture.DataOut.DSPStatus = (raw_datarx[9] << 8) + raw_datarx[8];
    mGesture.DataOut.GestureInfo = (raw_datarx[13] << 24) + (raw_datarx[12] << 16) + (raw_datarx[11] << 8) + raw_datarx[10];
    mGesture.DataOut.TouchInfo = (raw_datarx[17] << 24) + (raw_datarx[16] << 16) + (raw_datarx[15] << 8) + raw_datarx[14];
    mGesture.DataOut.AirWheelInfo = (raw_datarx[19] << 8) + raw_datarx[18];
    mGesture.DataOut.Position.X = (raw_datarx[21] << 8) + raw_datarx[20];
    mGesture.DataOut.Position.Y = (raw_datarx[23] << 8) + raw_datarx[22];
    mGesture.DataOut.Position.Z = (raw_datarx[25] << 8) + raw_datarx[24];
    nrf_gpio_pin_write(MGC_RDY_OUT,1);                                  //Release TS to new measurement
		
//    GPIO_Digital_Input (&GPIOD_BASE, _GPIO_PINMASK_10);
			nrf_gpio_cfg(MGC_RDY_IN,NRF_GPIO_PIN_DIR_INPUT,NRF_GPIO_PIN_INPUT_CONNECT,NRF_GPIO_PIN_NOPULL,NRF_GPIO_PIN_S0D1,NRF_GPIO_PIN_SENSE_LOW);
}